# Application mobile

Créer une application **mobile** sécurisée pour communiquer avec une api REST : 

Partie **publique**

- Une page de **login**
- Une page de **register**
- Une page d'oublie de **mot de passe**
- Une page de ré-initialisation de **mot de passe**

Partie **privée : 3 pages** 

- Pages
    - Page **home** → Liste tous les users actifs de l'applications
    - Page **profile** → Liste toutes vos informations
    - Page **Search** → Page avec un champ de recherche (email) avec les résultats de la recherche

- Un **header** indiquant :
    - Le **nom** de l'utilisateur actuel
    - Un bouton de **déconnexion**
- Une **navigation** disponible sur toutes les pages de la partie privée avec :
    - **Home**
    - **Profile**
    - **Search**

[Lien vers figma](https://www.figma.com/file/EKtGwOSFRms1v3y6gY2P0H/Untitled?node-id=4%3A190)
