// import React, { useState,useEffect } from 'react'
// import { Button, Text, View } from 'react-native'
// import PageRegister from './pages/PageRegister'
// import PageLogin from './pages/PagesLogin'
// import Allusers from './pages/AllUsers'

// import Profil from './components/profil'
// import AuthenticateMe from './components/authenticateMe'

// export default class App extends React.Component {

//   constructor(props) {
//     super(props)
//     this.state = {
//       data: [],
//       text: "Ceci" 
//     }
//   }
//   //this.setState({ text: "Cela"})

//   render() {

//     return (
//       <>
//       <View style={{flex:1, backgroundColor: 'black', flexDirection:'column', alignItems: 'center'}}>

//       <Text style={{fontSize:18,color:'white',padding:10}}>{this.state.text}</Text>
//       <Button title="change text" onPress={ () => this.setState({ text: "Cela" }) }/>

//         {/* <Navbar/> */}
//         <PageRegister/>
//         <PageLogin/>
//         {/* <Profil /> */}
//         <AuthenticateMe/>
//         <Allusers/>
//       </View>
//       </>
//     )
//   }
// }

import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from "@react-navigation/native-stack";

import Home from './components/home'
import Register from './components/register'
import Login from './components/login'
import Allusers from './components/allusers'
import AuthenticateMe from './components/authenticateMe'
import EmailProfil from './components/emailProfil'


const Stack = createNativeStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Home" component={Home} />
        <Stack.Screen name="Register" component={Register} />
        <Stack.Screen name="Login" component={Login} />
        <Stack.Screen name="Allusers" component={Allusers} />
        <Stack.Screen name="AuthenticateMe" component={AuthenticateMe} />
        <Stack.Screen name="EmailProfil" component={EmailProfil} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
