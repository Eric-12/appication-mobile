import React, { useState } from "react";
import {Text,View,TextInput,Button,Alert} from 'react-native'
import api from '../utils/api'
import styles from '../styles/styles'



const navbar = () => {
    return (
        <View style={styles.navViewBox}>
            <Text style={styles.navItem}>Inscription</Text>
            <Text style={styles.navItem}>Se connecter</Text>
        </View>
    )        
}



// Navigation/Navigation.js
// import { createStackNavigator } from 'react-navigation-stack'
// import Search from '../Components/Search'

// const SearchStackNavigator = createStackNavigator({
//   Search: { // Ici j'ai appelé la vue "Search" mais on peut mettre ce que l'on veut. C'est le nom qu'on utilisera pour appeler cette vue
//     screen: Search,
//     navigationOptions: {
//       title: 'Rechercher'
//     }
//   }
// })

// export default navbar