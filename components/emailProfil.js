import React, { useState, useEffect } from "react";
import { StyleSheet, View, FlatList, Text} from 'react-native';
import instanceAPI from '../utils/api';

const emailProfil = () => {

  const [user, setUser] = useState([]);
  const email = "lanzae32@gmail.com"

  const getUsers = async () => {
    const axiosResponse = await instanceAPI.get('/users/email/lanzae32@gmail.com');
    console.log(axiosResponse.data);
    setUser(axiosResponse.data)
    console.log(user)
  }

  useEffect(() => {
    getUsers();
  }, [])
  

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Profil à partir d'un email</Text>
      <View style={styles.viewBox}>
          <Text style={styles.item}>
            {user.firstname}
          </Text>
          <Text style={styles.item}>
            {user.lastname}
          </Text>
          <Text style={styles.item}>
            {user.username}
          </Text>
          <Text style={styles.item}>
            {user.email}
          </Text>
          <Text style={styles.item}>
            {user.password}
          </Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container:{
      flex:1,
      backgroundColor:'black',
  },
  title: {
      alignSelf:'center',
      marginTop: 50,
      fontSize: 20,
      fontWeight:'bold',
      color:'white',
  },
  viewBox: {
      padding:20,
      marginTop:20,
      paddingHorizontal: 20,
  },
  item: {
    margin:5,
    color: 'white',
    borderColor:'gray',
    borderWidth:1,

    padding: 10,
    fontSize: 18,
  },
  scrollView: {
    marginHorizontal: 20,
  }
});

export default emailProfil;