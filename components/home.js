import * as React from "react";
import { StyleSheet, Button, View } from "react-native";

function home({ navigation }) {
  return (
    <View style={styles.container}>
      <View style={styles.container_button}>
        <Button
          title="S'inscrire"
          color="black"
          onPress={() => navigation.navigate("Register")}
        />
      </View>

    <View style={styles.container_button}>  
      <Button 
        title="Se connecter"
        color="black"
        onPress={() => navigation.navigate("Login")}
      />
    </View>

    <View style={styles.container_button}>  
      <Button 
        title="Voir tout les utilisateurs"
        color="black"
        onPress={() => navigation.navigate("Allusers")}
      />
    </View>

    <View style={styles.container_button}>  
      <Button 
        title="Persistence utilisateur"
        color="black"
        onPress={() => navigation.navigate("AuthenticateMe")}
      />
    </View>

    <View style={styles.container_button}>  
      <Button 
        title="Profil à partir d'un email"
        color="black"
        onPress={() => navigation.navigate("EmailProfil")}
      />
    </View>

  </View>

  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor:'black',
    color:'white',
    justifyContent: "center",
    paddingHorizontal: 30
  },
  container_button: {
    borderWidth:1,
    borderColor:'white',
    borderRadius:5,
    margin: 10
  },
});

export default home;
