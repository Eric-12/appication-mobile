// components/Login.js

import React, { useState,useEffect } from "react";
import {StyleSheet, Text, View, TouchableOpacity} from 'react-native'
import instanceAPI from '../utils/api'
import {getLocalStorage} from '../utils/localStorage'

/*
-Une fois logger
-Lors d’une requête sur notre API, celle-ci doit comporter à la fois le cookie contenant le JWT et un en-tête HTTP x-xsrf-token contenant le token CSRF;
-Notre API décode le JWT et vérifie sa validité;
-Notre API vérifie que le token CSRF contenu dans l’en-tête HTTP correspond à celui du payload du JWT, si c’est le cas l’utilisateur est authentifié.
*/
const authenticateMe  = () => {

    const [error, setError] = useState(null);
    const [fieldError, setFieldError] = useState(null);
    const [isLoading, setIsLoading] = useState(false);
    const [redirect, setRedirect] = useState(false);
    const [is_connected, setIs_connected] = useState("Déconnecté");
    

    const handleSubmit = async (e) => {
        setError(null)
        setFieldError(null)
        setIsLoading(false)

        console.log("S'authentifier avec le token");

        try {
            /* On récupère le token CSRF depuis le localStorage stocké les données sous forme de chaines de caractères */
            /* Nous transformons donc la donnée en JSON avec la fonction getLocalStorage */
            const xsrfToken = await getLocalStorage();
            if (!xsrfToken) {
                /* Traitement dans le cas où le token CSRF n'existe dans le localStorage */
                console.log('Oups pas trouvé le token')
            }
            // console.log('token : ' + JSON.stringify(xsrfToken))
            // console.log(JSON.stringify('xsrfToken : ' + xsrfToken.xsrfToken))
            // console.log(JSON.stringify('accessToken : ' + xsrfToken.accessToken))


            // On créer l'en-tête avec le x-xsrf-token et l'accessToken que l'on extrait du token
            instanceAPI.defaults.headers.common["x-xsrf-token"] = xsrfToken.xsrfToken;
            instanceAPI.defaults.headers.common["Authorization"] = "Bearer " + xsrfToken.accessToken;
 
            
            // On effectue la requête
            const result = await instanceAPI.get('/auth/me/');
            console.log(result);

            if (result.status === 200) {
                console.log(result);
                setIs_connected("Connecté");
                // setRedirect(true)
            }

        } catch (err) {
            setError(err.response.data.message)
            console.log('erreur : ' + error)
        }
    }

    return (
        <View style={styles.container}>
            <Text style={styles.title}>Auth me</Text>
            <View style={styles.viewBox}>
                <TouchableOpacity
                    style={styles.container_button}
                    onPress={() => handleSubmit()}
                >
                    <Text style={styles.button}>M'authentifier</Text>
                </TouchableOpacity>
                <Text style={styles.label}>{is_connected}</Text>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:'black',
    },
    title: {
        alignSelf:'center',
        marginTop: 50,
        fontSize: 20,
        fontWeight:'bold',
        color:'white',
    },
    viewBox: {
        padding:20,
        marginTop:20,
        paddingHorizontal: 20,
    },
    textinput: {
        backgroundColor:'black',
        borderWidth:1,
        borderRadius: 5,  
        borderColor:'white',
        marginVertical:5,
        padding:10,
        height: 35,
        color:'white',
    },
    container_button: {
        marginTop:10,
        padding:10,
        borderRadius:5,
        borderWidth:1,
        borderColor:'#007bff',
        width:150,
        justifyContent:'center',
        alignSelf : 'center',

    },
    button:{
        color: '#007bff',
        alignSelf : 'center',
        fontSize:18,
    },
    label:{
        padding:10,
        alignSelf:'center',
        color:'white',
        fontSize: 16,
    },
  });

export default authenticateMe