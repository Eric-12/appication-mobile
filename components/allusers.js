import React, { useState,useEffect } from "react";
import {StyleSheet, View, FlatList, Text, SafeAreaView, ScrollView } from 'react-native';
import instanceAPI from '../utils/api'

const allusers = () => {

  const [users, setUsers] = useState([]);

  const getUsers = async () => {
    const axiosResponse = await instanceAPI.get('/users/public');
    console.log(axiosResponse.data);
    setUsers(axiosResponse.data)
  }

  useEffect( () => {
    getUsers();
  }, [])
  

  return (
      <SafeAreaView style={styles.container}>
        <Text style={styles.title}>Liste de tous les utilisateurs</Text>
        <ScrollView style={styles.scrollView}>
          {users.map((user)=>(
            <Text style={styles.item}>
            {user.firstname + ' ' + user.lastname}
          </Text>
          ))}
        </ScrollView>
      </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container:{
    flex:1,
    backgroundColor:'black',
    // paddingTop: StatusBar.currentHeight,
  },
  viewBox: {
    padding:20,
    marginTop:20,
    paddingHorizontal: 20,
  },
title: {
  alignSelf:'center',
  marginTop: 50,
  fontSize: 20,
  fontWeight:'bold',
  color:'white',
},
scrollView: {
  backgroundColor: 'black',
  marginHorizontal: 20,
},
item: {
  margin:5,
  color: 'white',
  borderColor:'gray',
  borderWidth:1,
  padding: 10,
  fontSize: 18,
}
});


export default allusers;