// components/Register.js

import React, { useState } from "react";
import {StyleSheet, Text, View, TextInput, Alert, TouchableOpacity} from 'react-native'
import instanceAPI from '../utils/api'


const register = () => {

    const [error, setError] = useState(null);
    const [fieldError, setFieldError] = useState(null)
    const [isLoading, setIsLoading] = useState(false)
    const [redirect, setRedirect] = useState(false)

    const [firstname, setFirstname] = useState("");
    const [lastname, setLastname] = useState("");
    const [password, setPassword] = useState("");
    const [username, setLUsername] = useState("");
    const [email, setEmail] = useState("");

    const test = ()=>{
        console.log('Simple Button pressed')
        Alert.alert('Simple Button pressed')
    }

    //Logic du call d'API à faire pour la partie REGISTER
    const body = {
        firstname,
        lastname,
        password,
        username,
        email
    }

    // REQUETE POST ASYNCHRONE VERS API REGISTER
    const handleSubmit = async (e) => {

        setError(null)
        setFieldError(null)
        setIsLoading(false)

        try {
            const result = await instanceAPI.post('/users/', body);
            if (result.status === 201) {
                setRedirect(true)
            }
        } catch (err) {
            setError(err.response.data.message)
            console.log('erreur : ' + error)
            Alert.alert(error)
        }
    }

    return (
        <View style={styles.container}>
            <Text style={styles.title}>Inscription</Text>
            <View style={styles.viewBox}>
                <TextInput onChangeText={setLastname} style={styles.textinput} placeholderTextColor="gray" placeholder='Nom'/>
                <TextInput onChangeText={setFirstname} style={styles.textinput} placeholderTextColor="gray" placeholder='Prénom'/>
                <TextInput onChangeText={setPassword} style={styles.textinput} placeholderTextColor="gray" placeholder='Mot de passe'/>
                <TextInput onChangeText={setLUsername} style={styles.textinput} secureTextEntry={true} placeholderTextColor="gray" placeholder='Pseudo'/>
                <TextInput onChangeText={setEmail} style={styles.textinput} placeholderTextColor="gray" placeholder='Email'/>
                <TouchableOpacity
                    style={styles.container_button}
                    onPress={() => handleSubmit()}
                >
                    <Text style={styles.button}>S'inscrire</Text>
                </TouchableOpacity>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:'black',
    },
    title: {
        alignSelf:'center',
        marginTop: 50,
        fontSize: 20,
        fontWeight:'bold',
        color:'white',
    },
    viewBox: {
        padding:20,
        marginTop:20,
        paddingHorizontal: 20,
    },
    textinput: {
        backgroundColor:'black',
        borderWidth:1,
        borderRadius: 5,  
        borderColor:'white',
        marginVertical:5,
        padding:10,
        height: 35,
        color:'white',
    },
    container_button: {
        marginTop:10,
        padding:10,
        borderRadius:5,
        borderWidth:1,
        borderColor:'#007bff',
        width:150,
        justifyContent:'center',
        alignSelf : 'center',

    },
    button:{
        color: '#007bff',
        alignSelf : 'center',
        fontSize:18,
    }
  });
  

export default register

