// components/Login.js

import React, { useState } from "react";
import {StyleSheet, Text, View, TextInput,  TouchableOpacity} from 'react-native'
import instanceAPI from '../utils/api'
import {setLocalStorage} from '../utils/localStorage'

/*
-Lors de l’authentification, notre API va générer un token unique que l’on appelle token CSRF et qui sera stocké dans le payload du JWT;
-L'API envoie le JWT dans un cookie et le token CSRF dans le corps de la réponse HTTP;
-Le navigateur va s’occuper de stocker le cookie et nous allons stocker le token CSRF dans le localStorage;
*/

const login  = () => {

    const [error, setError] = useState(null);
    const [fieldError, setFieldError] = useState(null);
    const [isLoading, setIsLoading] = useState(false);
    const [redirect, setRedirect] = useState(false);

    const [password, setPassword] = useState("123456");
    const [email, setEmail] = useState("dwwm2019@gmail.com");
    

    const handleSubmit = async (e) => {

        setError(null)
        setFieldError(null)
        setIsLoading(false)

        console.log('login');

        // Data to send with request
        const body = {
            email,
            password
        }

        try {
            const result = await instanceAPI.post('/auth/login', body);

            if (result.status === 200) {
                // stocke le token CSRF dans le localStorage
                setLocalStorage(result.data)
                setRedirect(true)
                console.log('login OK')
            }
        } catch (err) {
            setError(err.response.data.message)
            console.log('erreur : ' + error)
            Alert.alert(error)
        }
    }

    return (
    <View style={styles.container}>
        <Text style={styles.title}>Se connecter</Text>
        <View style={styles.viewBox}>
            <TextInput style={styles.textinput} value={email} onChangeText={setEmail} placeholderTextColor="gray" placeholder='Email'/>
            <TextInput style={styles.textinput} secureTextEntry={true} value={password} onChangeText={setPassword} placeholderTextColor="gray" placeholder='Mot de passe'/>
            <TouchableOpacity
                    style={styles.container_button}
                    onPress={() => handleSubmit()}
                >
                    <Text style={styles.button}>Se connecter</Text>
                </TouchableOpacity>
            <Text style={styles.noPassword} onPress={ () => { }} >Mot de passe oublié</Text>
        </View>
    </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:'black',
    },
    title: {
        alignSelf:'center',
        marginTop: 50,
        fontSize: 20,
        fontWeight:'bold',
        color:'white',
    },
    viewBox: {
        padding:20,
        marginTop:20,
        paddingHorizontal: 20,
    },
    textinput: {
        backgroundColor:'black',
        borderWidth:1,
        borderRadius: 5,  
        borderColor:'white',
        marginVertical:5,
        padding:10,
        height: 35,
        color:'white',
    },
    container_button: {
        marginTop:10,
        padding:10,
        borderRadius:5,
        borderWidth:1,
        borderColor:'#007bff',
        width:150,
        justifyContent:'center',
        alignSelf : 'center',

    },
    button:{
        color: '#007bff',
        alignSelf : 'center',
        fontSize:18,
    },
    noPassword:{
        fontWeight:'bold'
    }
  });

export default login