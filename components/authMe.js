import React, { useState,useEffect } from "react";
import {Text,View,TextInput,Button} from 'react-native'
import styles from '../styles/styles'
import instanceAPI from '../utils/api'
import {getLocalStorage} from '../utils/localStorage'


const authMe = () => {

    const [error, setError] = useState(null);
    const [fieldError, setFieldError] = useState(null);
    const [isLoading, setIsLoading] = useState(false);
    const [is_connected, setIs_connected] = useState("Déconnecté");

    useEffect(()=>{
        console.log('vous avez cliquer')
        handleSubmit()
        return () => {
            // Fonction de rappelle qui s'exécute lors du démontage
        };
    },[])

    const handleSubmit = async (e) => {
        setError(null)
        setFieldError(null)
        setIsLoading(false)

        console.log("S'authentifier avec le token");

        try {
            /* On récupère le token CSRF depuis le localStorage stocké les données sous forme de chaines de caractères */
            /* Nous transformons donc la donnée en JSON avec la fonction getLocalStorage */
            const xsrfToken = await getLocalStorage();
            if (!xsrfToken) {
                /* Traitement dans le cas où le token CSRF n'existe dans le localStorage */
                console.log('Oups pas trouvé le token')
            }

            // On créer l'en-tête avec le x-xsrf-token et l'accessToken que l'on extrait du token
            instanceAPI.defaults.headers.common["x-xsrf-token"] = xsrfToken.xsrfToken;
            instanceAPI.defaults.headers.common["Authorization"] = "Bearer " + xsrfToken.accessToken;

            
            // On effectue la requête
            const result = await instanceAPI.get('/auth/me/');
            console.log(result);

            if (result.status === 200) {
                console.log(result);
                setIs_connected("Connecté");
            }

        } catch (err) {
            setError(err.response.data.message)
            console.log('erreur : ' + error)
        }
    }
}

export default authMe