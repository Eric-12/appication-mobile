import {StyleSheet, TouchableWithoutFeedbackBase} from 'react-native'

const styles = StyleSheet.create({
    navViewBox:{
        alignSelf: "stretch",
        flexDirection: 'row',
        padding:20,
        borderWidth:1,
        borderRadius:5,
        borderColor:'white',
        backgroundColor:'gray'
    },
    navItem: {
        fontSize:20,
        color: 'white',
        padding: 20,
    },
    viewBox: {
        padding:20,
        marginTop:20,
        width:500,
        borderWidth:1,
        borderRadius:5,
        borderColor:'white',
        //backgroundColor: '#BAB9E7',
    },
    title: {
        color:'white',
        marginBottom: 20,
        fontSize: 32,
    },
    label:{
        padding:10,
        alignSelf:'center',
        color:'white',
        fontSize: 16,
    },
    fixToText: {
        flexDirection: 'row',
        justifyContent: 'space-between',
      },
      separator: {
        marginVertical: 8,
        borderBottomColor: '#737373',
        borderBottomWidth: StyleSheet.hairlineWidth,
      },
    textinput: {
        backgroundColor:'white',
        borderWidth:1,
        borderRadius: 5,  
        borderColor:'white',
        marginLeft: 5,
        marginRight: 5,
        marginBottom: 10,
        height: 40,
        paddingLeft: 5,
        color:'black',
    },
    button: {
        marginTop:10,
        color: 'yellow',
        width:200,
        alignSelf : 'center'
    },
    noPassword:{
        padding:10,
        fontWeight:'bold',
        alignSelf: 'center',
        fontSize: 14,
        color: 'white'
    }
})

export default styles